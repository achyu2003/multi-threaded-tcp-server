# Multi-Threaded-TCP-Server



## Multi-threaded TCP Server for Jeopardy Game

A multi-threaded TCP server is a type of server that can handle multiple client connections simultaneously using multiple threads. In this architecture, each client connection is handled by a separate thread, allowing the server to handle multiple requests at the same time. This approach is commonly used in network programming to improve the performance and scalability of server applications.

## Jeopardy Game Rules
- Contestants must phrase their responses in the form of a question.
- Contestants must use a buzzer in order to indicate their responses. For the purpose of this game, we assume that all the contestants must buzz before the game master declared the result
- If the contestant’s response is correct, he/she moves to the next round. While, if it’s incorrect, the contestant is declared out of the game.
- The contestant with the highest number of correct answers is the winner.

Analogous to this game, we have implemented it in the form of a Multi-threaded TCP server .

## Outputs
1. ![Server waiting for clients to subscribe](images/image10.png)
2. ![Clients Subscribing](images/image18.png)
3. ![Clients Buzzing in](images/image5.png)
4. ![First client who buzzes getting to answer](images/image2.png)
5. ![Server letting second person in the queue to answer if the answer given was wrong](images/image1.png)
6. ![Server displays end of game](images/image4.png) 


## Usage
Python should be installed on your system. 
Run the ``server.py`` first, followed by ``client.py``.

Note that the client file should be run three times.

## Contributing
Open to contribution

## Authors and acknowledgment
Authors -> Aditya R.G, Archanaa A Chandaragi, Anushka S

## Project status
The project is under development.
